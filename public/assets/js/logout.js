// console.log("hello")

// upon logging out the credentials of the user should be removed from the local storage

localStorage.clear();

//the clear method will wipe out/remove the contents of the local storage

//upon clearning out the local storage, redirect the user back to the login page.

window.location.replace("./login.html")

// INDIVIDUAL TASKS:

// apply all the necessary changes in the navbar of each page

// STRETCH GOAL:
// create a logic that will disable the profile link navbar component if an admin is logged in. 
// the admin should be redirected to the courses page only.