// console.log("hello from js")

//Lets identify which course this page needs to display inside the browser. 

//so earlier we passed the course id data inside the parameters of the url for us to quickly identify which course to display. 

//how are we going to get the value passed inside the URL params?

//easy..the answer use a URLSearchParams() -> this method/constructor creates and returns a URLSearchParams object. 

//window.location -> returns a location object with information about the "current" location of the document. 
//.search -> contains the query string section of the current url.
let urlValues = new URLSearchParams(window.location.search)

//lets check what the structure of this variable will look like
//console.log(urlValues) //checker

//lets get only the desired data from the object using a get()
let id = urlValues.get('courseId')
console.log(id) //checker

//lets capture the value of the access token inside the local storage
let token = localStorage.getItem('token')
//console.log(token) this is just for checking

//lets set first a container for all the details that we would want to display. 
let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let desc = document.querySelector("#courseDesc")
let enroll = document.querySelector("#enrollmentContainer")

//lets send a request going to the endpoint that will allow us to get and display the course details 
fetch(`https://lit-reef-53083.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(convertedData => {
  console.log(convertedData)

  name.innerHTML = convertedData.name
  price.innerHTML = convertedData.price
  desc.innerHTML = convertedData.description
  enroll.innerHTML = `<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>`

  document.querySelector("#enrollButton").addEventListener("click", () => {
    //insert the course inside the enrollments array of the user
    fetch('https://lit-reef-53083.herokuapp.com/api/users/enroll', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`//we have to get the actual value of the token variable
      },
      body: JSON.stringify({
        courseId: id
      })
    })
      .then(res => {
        return res.json()
      }).then(convertedResponse => {
        console.log(convertedResponse)
        //lets create a control structure that will determine a response according to the result that will diplayed to the client.
        if (convertedResponse === true) {
          alert("Thank you for enrolling")

          //we can redirect the user back to the courses page
          window.location.replace("./courses.html")
        } else {
          //inform the user if the enrollment failed. 
          alert("something went wrong!")
        }
      })
  })
})

//send a demo video of the current output displaying the details about a selected course
//10 mins.





// // console.log("hello from js")

// //Lets identify which course this page needs to display inside the browser. 

// //so earlier we passed the course id data inside the parameters of the url for us to quickly identify which course to display. 

// //how are we going to get the value passed inside the URL params?

// //easy..the answer use a URLSearchParams() -> this method/constructor creates and returns a URLSearchParams object. 

// //window.location -> returns a location object with information about the "current" location of the document. 
// //.search -> contains the query string section of the current url.
// let urlValues = new URLSearchParams(window.location.search)

// //lets check what the structure of this variable will look like
// //console.log(urlValues) //checker

// //lets get only the desired data from the object using a get()
// let id = urlValues.get('courseId')
// let userId = localStorage.getItem("id");
// console.log(id) //checker

// //lets capture the value of the access token inside the local storage
// let token = localStorage.getItem('token')
// //console.log(token) -->just for checking

// //lets set first a container for all the details that we would want to display. 
// let name = document.querySelector("#courseName")
// let price = document.querySelector("#coursePrice")
// let desc = document.querySelector("#courseDesc")
// let enroll = document.querySelector("#enrollmentContainer")

// //lets send a request going to the endpoint that will allow us to get and display the course details 
// fetch(`https://lit-reef-53083.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(convertedData => {
//   console.log(convertedData)

//   name.innerHTML = convertedData.name
//   price.innerHTML = convertedData.price
//   desc.innerHTML = convertedData.description
//   enroll.innerHTML = `<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>`

//   document.querySelector("#enrollButton").addEventListener("click", () => {
//     /*Swal.fire({
//       icon: "success",
//       text:"Congrats! You have successfully enrolled to a course!"})*/

//     //insert the course inside the enrollments array of the user
//     console.log("id: " + id)
//     console.log("userId: " + userId)
//     fetch('https://lit-reef-53083.herokuapp.com/api/users/enroll', {
//       method: 'POST',
//       headers: {
//         'Content-Type': 'application/json',
//         'Authorization': `Bearer ${token}`//we have to get the actual value of the token variable
//       },
//       body: JSON.stringify({
//         courseId: id,
//         userId: userId
//       })
//     })
//       .then(res => {
//         return res.json()
//       }).then(convertedResponse => {
//         alert(`${convertedResponse.message}`)
//         //lets create a control structure that will determine a response accdg to thr result that will be displayed to the client

//         // if (convertedResponse === true) {
//         // 	alert(`${convertedResponse.message}`)

//         // 	//we can redirect the user back to the courses page
//         // 	window.location.replace("./courses.html")
//         // } else {
//         // 	//inform the user if the enrollment failed.
//         // 	alert("Oh no, something went wrong!")
//         // }
//       })
//   })
// })


