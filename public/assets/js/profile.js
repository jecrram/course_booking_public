let token = localStorage.getItem("token");

let container = document.querySelector("#profileContainer");

// container.innerHTML = `test`
//our goal here is to display the information about the user details
//send a request to the backend project

fetch("https://lit-reef-53083.herokuapp.com/api/users/details", {
  method: "GET",
  headers: {
    "Content-Type": "application/json",
    Authorization: `Bearer ${token}`,
  },
})
  .then((res) => res.json())
  .then((jsonData) => {
    //lets create a checker to make sure the fetch is successful
    // console.log(jsonData)

    let userSubjects = jsonData.enrollments
      .map((course) => {
        return fetch(`https://lit-reef-53083.herokuapp.com/api/courses/${course.courseId}`)
          .then((res) => res.json())
          .then((data) => {
            return `
            <tr>
                <td> ${data.name} </td>
                <td> ${data.description} </td>
                <td> ${data.price} </td>
            </tr>
            `;
          });
      })
      .join("");

    console.log(userSubjects);
    //how are we going to display the information inside the front end component?
    //lets target the div element first using its id attribute
    //lets create a section to display all the courses the user is enrolled in
    container.innerHTML = `
        <link rel="stylesheet" href="./../css/stylepages.css">

          <div class="col-md-4">
              <div class="card">
                  <div class="card-body">
                  <div class="row">
                  <div class="col"><h5 class="card-title">First Name</h5></div>
              <div class="col">${jsonData.firstName}</div>
          </div>
          <div class="row">
          <div class="col"><h5 class="card-title">Last Name</h5></div>
          <div class="col">${jsonData.lastName}</div>
          </div>
          <div class="row">
          <div class="col"><h5 class="card-title">Email</h5></div>
          <div class="col">${jsonData.email}</div>
          </div>
          <div class="row">
          <div class="col"><h5 class="card-title">Mobile Number</h5></div>
          <div class="col">${jsonData.mobileNo}</div>
          </div>
          </div>
          <div class="card-footer">
          <table class="table">
          <tr>
          <th>Course ID:</th>
          <th>Enrolled On:</th>
          <th>Status: </th>
          <tbody> ${userSubjects} </tbody>
          </tr>
          </table>
          </div>
          </div>
          </div>
      `;
  });

// //console.log("hello from js")
// //get the value of the access token inside the local storage and place it inside a new variable
// let token = localStorage.getItem("token");
// console.log(token)

// let lalagyan = document.querySelector("#profileContainer")

// fetch('http://localhost:4000/api/users/details', {
//   method: 'GET',
//   headers: {
//     'Content-Type': 'application/json',
//     'Authorization': `Bearer ${token}`
//   }
// }).then(res => res.json())
//   .then(jsonData => {
//     //lets create a checker to make sure the fetch is successful

//     //how are we going to display the information inside the front end component?
//     //lets target the div element first using its id attribute
//     lalagyan.innerHTML =
//       `
// 			<div class="col-md-13">
// 				<section class="jumbotron my-5">
// 					<h3 class="text-center">First Name: ${jsonData.firstName}</h3>
// 					<h3 class="text-center">Last Name: ${jsonData.lastName}</h3>
// 					<h3 class="text-center">Email: ${jsonData.email}</h3>
// 					<h3 class="text-center">Mobile Number: ${jsonData.mobileNo}</h3>
// 					<table class="table">
// 						<tr>
// 							<th>Course Name:</th>
// 							<th>Enrolled on:</th>
// 							<th>Status: </th>
// 							<tbody id="courseBody"></tbody>
// 						</tr>
// 					</table>

// 				</section>
// 			</div>
// 	`

//     for (let i = 0; i < jsonData.enrollments.length; i++) {
//       fetch(`http://localhost:4000/api/courses/${jsonData.enrollments[i].courseId}`, {
//         method: 'GET',
//         headers: {
//           'Content-Type': 'application/json',
//           'Authorization': `Bearer ${token}`
//         }
//       })
//         .then(res => res.json())
//         .then(courseData => {
//           document.getElementById('courseBody').innerHTML +=
//             `
// 				<tr>
// 					<td>${courseData.name}</td>
// 					<td> ${jsonData.enrollments[i].enrolledOn}</td>
// 					<td> ${jsonData.enrollments[i].status}</td>
// 				</tr>
// 			`
//         });
//     }
//   })

//get the value of the access token inside the local storage and place it inside a new variable. 
// let token = localStorage.getItem("token");
// // console.log(token)

// let lalagyan = document.querySelector("#profileContainer")
// //our goal here is to display the information about the user details 
// //send a request to the backend project
// fetch('https://lit-reef-53083.herokuapp.com/api/users/details', {
//   method: 'GET',
//   headers: {
//     'Content-Type': 'application/json',
//     'Authorization': `Bearer ${token}`
//   }
// }).then(res => res.json())
//   .then(jsonData => {
//     //lets create a checker to make sure the fetch is successful
//     //console.log(jsonData)

//     //get all the elements inside the enrollments array.
//     console.log(jsonData.enrollments)
//     let userSubjects = jsonData.enrollments.map(subject => {
//       console.log(subject)
//       return (
//         //i want to create a container that will serve as the html boilerplate that will display the properties of the subject that want to get.

//         //analyze all the data that i already have and how am i going to use thatb information to solve the task.  
//         `
//           <tr>
//             <td> ${subject.courseId}</td>
//             <td> ${subject.enrolledOn}</td>
//             <td> ${subject.status}</td>
//           </tr>
//         `
//       )
//     }).join("") //this will remove the commas


//     //how are we going to display the information inside the front end component? 
//     //lets target the div element first using its id attribute
//     //lets create a section to display all the courses the user is enrolled in
//     lalagyan.innerHTML = `
//        <div class="col-md-13">
//         	<section class="jumbotron my-5">
//        		   <h3 class="text-center">First Name: ${jsonData.firstName}</h3>
//        		   <h3 class="text-center">Last Name: ${jsonData.lastName}</h3>
//        		   <h3 class="text-center">Email:     ${jsonData.email}</h3>
//        		   <h3 class="text-center">Mobile Number:  ${jsonData.mobileNo}</h3>
//                <table class="table">
//                	<tr>
//                		<th>Course ID:</th>
//                		<th>Enrolled On:</th>
//                		<th>Status: </th>
//                		<tbody> ${userSubjects} </tbody>
//                	</tr>
//                </table>

//         	</section>
//        </div>

//     `
//   })

// //7:45 PM to work with your group and produce an output
// //a demo video submitted in HO of your demonstrating the requirements. 
