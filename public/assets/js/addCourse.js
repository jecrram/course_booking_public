//create our add course page 
let formSubmit = document.querySelector('#createCourse')


//create a logic that will make sure that the needed information is sufficient from our form component. and make sure to inform the user what went wrong.   
//15mins submit another demo video that will display this new feature. 


//lets acquire an event that will applied in our form component.
//create a subfunction inside the method to describe the procedure/action that will take place upon triggering the event.
formSubmit.addEventListener("submit", (mangyayari) => {
    mangyayari.preventDefault() //this will avoid page redirection

    //lets target the values of each component inside the forms. 
    let name = document.querySelector("#courseName").value
    let cost = document.querySelector("#coursePrice").value
    let desc = document.querySelector("#courseDesc").value

    // lets create a checker to see if we were able to capture the values of each input fields. 
    console.log(name)
    console.log(cost)
    console.log(desc)

    // send a request to the backend project to process the data for creating a new entry inside our courses collection. 
    // if (name !== "" && cost !== "" && desc !== "") {
    //     //should i allow the user to create a new document?
    //     fetch('http://localhost:4000/api/courses/create', {
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': 'application/json'
    //         },
    //         body: JSON.stringify({
    //             //what are the properties of the document that the user needs to fill?
    //             //how can we pass the values of the form component inside our request body? 
    //             name: name,
    //             description: desc,
    //             price: cost
    //         })
    //     }).then(res => {
    //         console.log(res.body)
    //         //to answer the question below lets observe first the structure of the response
    //         //console.log(res) 
    //         return res.json() //why? why do we need to convert the response into a json format? 
    //     }).then(info => {
    //         //console.log(info)
    //         //lets create a control structure that will describe the response of the UI to the client. 
    //         if (info === true) {
    //             Swal.fire("Success") //CREATE the appropriate message that should appear in this box when the condition has been met and vise versa. 
    //             //10 mins to produce the demo video for the create course page.
    //         } else {
    //             Swal.fire("Fail")
    //         }
    //         //lets check what the response looks like pag na applyan na siya ng json()
    //     })//how can we handle the promise object that will be returned once the fetch method event has happened?  	
    // } else {
    //     alert("Something went wrong! check the input fields.")
    // }
    //upon creating this fetch api request we are instantiating a promise 





    if (name !== "" && cost !== "" && desc !== "") {
        fetch('https://lit-reef-53083.herokuapp.com/api/courses/course-exists', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name
            })
        }).then(res => {
            // console.log(name)
            return res.json()
        }).then(convertedData => {
            // console.log(convertedData)
            if (convertedData === false) {
                fetch('https://lit-reef-53083.herokuapp.com/api/courses/create', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        //what are the properties of the document that the user needs to fill?
                        //how can we pass the values of the form component inside our request body? 
                        name: name,
                        description: desc,
                        price: cost
                    })
                }).then(res => {
                    //to answer the question below lets observe first the structure of the response
                    //console.log(res) 
                    console.log(res)
                    return res.json() //why? why do we need to convert the response into a json format? 
                }).then(info => {
                    //console.log(info)
                    //lets create a control structure that will describe the response of the UI to the client. 
                    if (info === true) {
                        Swal.fire("Success") //CREATE the appropriate message that should appear in this box when the condition has been met and vise versa. 
                        //10 mins to produce the demo video for the create course page.
                    } else {
                        Swal.fire("Fail")
                    }
                    //lets check what the response looks like pag na applyan na siya ng json()
                })//how can we handle the promise object that will be returned once the fetch method event has happened?  	
            }

            else {
                Swal.fire("Course already exists")
                // window.alert("Something went wrong! check the input fields.")
            }
        })
    } else {
        Swal.fire("Check details")
        // window.alert("Something went wrong! check the input fields.")
    }

})








