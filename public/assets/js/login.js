// console.log("hello from js")

//LETS target our form component inside our document. 
let loginForm = document.querySelector('#loginUser');

//the login form will be used by the client to insert his/her account to be authenticated by the app. 
loginForm.addEventListener("submit", (pagpasok) => {
   pagpasok.preventDefault()

   //lets capture the values our form components. 
   let email = document.querySelector("#userEmail").value
   let pass = document.querySelector("#password").value

   //lets create a checker to confirm the acquired values.
   // console.log(email)
   // console.log(pass)

   //our next task is to validate the data inside the input fields first.
   if (email == "" || pass == "") {
      alert("please input your email and/or address first")
   }
   else {
      //send a request to the desired enpoint.
      fetch("https://lit-reef-53083.herokuapp.com/api/users/login", {
         method: 'POST',
         headers: {
            'Content-Type': 'application/json'
         },
         body: JSON.stringify({
            email: email,
            password: pass
         })
      }).then(res => {
         // console.log(res)
         return res.json() //we are instantiating a promise (resolve, reject)
      }).then(dataConverted => {
         //check if conversion of the response has been successful
         //console.log(dataConverted.accessToken)
         //save the generated access token inside the local storage property of the browser. 
         if (dataConverted.accessToken) {
            localStorage.setItem('token', dataConverted.accessToken)
            // alert("successfully generated access token"); we just created this as confirmation of the previous task. 

            //Make sure that before you redirect the user to the next location you have to identify the access rights that you will grant for that user. 
            //how can we know if the user is an admin or not? 
            fetch('https://lit-reef-53083.herokuapp.com/api/users/details', {
               headers: {
                  //lets pass on the value of our access token. 
                  'Authorization': `Bearer ${dataConverted.accessToken}`
               } //upon sending this request we are instantiating a promise that can lead to 2 possible outcomes..what do we need to do to handle the possible outcome states of this promise?
            }).then(res => {
               return res.json()
            }).then(data => {
               //check if we are able to get the user's data
               //console.log(data)
               //fetching the user details/ info is a success
               //save the "id", "isAdmin"
               localStorage.setItem("id", data._id)
               localStorage.setItem("isAdmin", data.isAdmin)
               //as developers its up to you if you want to create a checker to make sure that all values are saved properly inside the web storage
               //console.log("items are set inside the local storage")  
               window.location.replace('./profile.html')
            })
            //how would you redirect the user to another location in your app?

         } else {
            //this block of code will run if no access token was generated
            alert("No access token generated")
            window.location.replace('./login.html')
         }
      })
   }
})

console.log("Hello!")