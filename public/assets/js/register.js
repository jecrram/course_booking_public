// console.log("hello from JS file"); 
let password = document.getElementById("password1");
let letter = document.getElementById("letter");
let capital = document.getElementById("capital");
let number = document.getElementById("number");
let length = document.getElementById("length");
let samePw = document.getElementById("samePw")

// When the user clicks on the password field, show the message box
password.onfocus = function () {
  document.getElementById("message").classList.add("d-flex");
  console.log("message");
}


// When the user starts to type something inside the password field
password.onkeyup = function () {
  // Validate lowercase letters
  var letters = /[a-zA-Z]/g;
  if (password.value.match(letters)) {
    letter.classList.remove("invalid");
    letter.classList.add("valid");
  } else {
    letter.classList.remove("valid");
    letter.classList.add("invalid");
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if (password.value.match(numbers)) {
    number.classList.remove("invalid");
    number.classList.add("valid");
  } else {
    number.classList.remove("valid");
    number.classList.add("invalid");
  }

  // Validate length
  if (password.value.length >= 8 && password.value.length < 16) {
    length.classList.remove("invalid");
    length.classList.add("valid");
  } else {
    length.classList.remove("valid");
    length.classList.add("invalid");
  }
}

//lets target first our form component and place it inside a new variable
let registerForm = document.querySelector("#registerUser")

//inside the first param of the method describe the event that it will listen to, while inside the 2nd parameter lets describe the action/ procedure that will happen upon triggering the said event. 
registerForm.addEventListener("submit", (pangyayari) => {
  pangyayari.preventDefault() //to avoid page refresh/page redirection once that the said event has been triggered. 

  //capture each values inside the input fields first, then repackage them inside a new variable
  let fName = document.querySelector("#firstName").value
  //lets create a checker to make sure that we are successful in captring the values.
  //console.log(firstName)
  let lastName = document.querySelector("#lastName").value
  //lets create a checker to make sure that we are successful in captring the values.
  //console.log(lastName)
  let email = document.querySelector("#userEmail").value
  //lets create a checker to make sure that we are successful in captring the values.
  //console.log(email)
  let mobileNo = document.querySelector("#mobileNumber").value
  //lets create a checker to make sure that we are successful in captring the values.
  //console.log(mobileNo)
  let password = document.querySelector("#password1").value
  //lets create a checker to make sure that we are successful in captring the values.
  //console.log(password)
  let verifyPassword = document.querySelector("#password2").value
  //lets create a checker to make sure that we are successful in captring the values.
  //console.log(verifyPassword)



  //lets create a data validation for our register page. WHY?
  ///why do we need to validate data? ..to check and verify if the data that we will accept is accurate. 
  //we do data validation to make sure that the storage space will be properly utilizes.

  //the folllowing info/data that we can validate 
  //email, password. mobileNo. 
  //lets create a control structure to determine the next set of procedures before the user can register a new account.
  //it will be a lot more efficient for you to sanitize the data before submitting it to the backend/server for processing

  //stretch goals: (these are not required)
  //=> the password should contain both alphanumeric characters and atleast 1 special character. 

  if ((fName !== "" && lastName !== "" && email !== "" && password !== "" && verifyPassword !== "") && (password === verifyPassword) && (password.length >= 8 && password.length <= 15) && (mobileNo.length === 11)) {
    //how are we going to integrate our email-exists method? 
    //we are going to send out a new request 
    //before you allow the user to create a new account check if the email value is still available for use. this will ensure that each user will have their unique user email 
    //upon sending this request we are instantiating a promise object, which means we should apply a method to handle the response. 
    fetch('https://lit-reef-53083.herokuapp.com/api/users/email-exists', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email
      })
    }).then(res => {
      console.log(email)
      return res.json() //to make it readable once the response returns to the client side. 
    }).then(convertedData => {
      // console.log(convertedData)
      //what would the response look like? 
      //lets create a control structure to determine the proper procedure depending on the response 
      if (convertedData === false) {
        // console.log(convertedData)
        //lets allow the user to register an account. 
        fetch('https://lit-reef-53083.herokuapp.com/api/users/register', {
          //we will now describe the structure of our request for register
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          //API only accepts request in a string format. 
          body: JSON.stringify({
            firstName: fName,
            lastName: lastName,
            email: email,
            mobileNo: mobileNo,
            password: password
          })
        }).then(res => {
          console.log(res)
          //console.log("hello");
          return res.json()
        }).then(data => {
          console.log(data)
          //lets create here a control structure to give out a proper response depending on the return from the backend. 
          if (data === true) {
            Swal.fire("New Account Registered Successfully!!!!")
          } else {
            //inform the user that something went wrong
            Swal.fire("Something with wrong during processing!")
          }
        }
        )
      } else {
        //lets inform the user what went wrong 
        Swal.fire("The Email Already exist!") // you can modify the message. 
      }
    }
    )

    //this block of code will run if the condition has been met
    //how can we create a new account for user using the data that he/she entered?
    //url -> describes the destination of the request. 
    //3 STATES for a promise (pending, fullfillment, rejected)
  } else {
    Swal.fire("Oh no!!!")
  }
})


console.log("hello from JS file");








//lets target first our form component and place it inside a new variable
// let registerForm = document.querySelector("#registerUser")

// //inside the first param of the method describe the event that it will listen to, while inside the 2nd parameter lets describe the action/ procedure that will happen upon triggering the said event. 
// registerForm.addEventListener("submit", (pangyayari) => {
//   pangyayari.preventDefault() //to avoid page refresh/page redirection once that the said event has been triggered. 

//   //capture each values inside the input fields first, then repackage them inside a new variable
//   let fName = document.querySelector("#firstName").value
//   //lets create a checker to make sure that we are successful in captring the values.
//   //console.log(firstName)
//   let lastName = document.querySelector("#lastName").value
//   //lets create a checker to make sure that we are successful in captring the values.
//   //console.log(lastName)
//   let email = document.querySelector("#userEmail").value
//   //lets create a checker to make sure that we are successful in captring the values.
//   //console.log(email)
//   let mobileNo = document.querySelector("#mobileNumber").value
//   //lets create a checker to make sure that we are successful in captring the values.
//   //console.log(mobileNo)
//   let password = document.querySelector("#password1").value
//   //lets create a checker to make sure that we are successful in captring the values.
//   //console.log(password)
//   let verifyPassword = document.querySelector("#password2").value
//   //lets create a checker to make sure that we are successful in captring the values.
//   //console.log(verifyPassword)


//   //lets create a data validation for our register page. WHY?
//   ///why do we need to validate data? ..to check and verify if the data that we will accept is accurate. 
//   //we do data validation to make sure that the storage space will be properly utilizes.

//   //the folllowing info/data that we can validate 
//   //email, password. mobileNo. 
//   //lets create a control structure to determine the next set of procedures before the user can register a new account.
//   //it will be a lot more efficient for you to sanitize the data before submitting it to the backend/server for processing

//   //stretch goals: (these are not required)
//   //=> the password should contain both alphanumeric characters and atleast 1 special character. 

//   if ((fName !== "" && lastName !== "" && email !== "" && password !== "" && verifyPassword !== "") && (password === verifyPassword) && (mobileNo.length === 11)) {
//     //how are we going to integrate our email-exists method? 
//     //we are going to send out a new request 
//     //before you allow the user to create a new account check if the email value is still available for use. this will ensure that each user will have their unique user email 
//     //upon sending this request we are instantiating a promise object, which means we should apply a method to handle the response. 
//     fetch('https://lit-reef-53083.herokuapp.com/api/users/email-exists', {
//       method: 'POST',
//       headers: {
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify({
//         email: email
//       })
//     }).then(res => {
//       return res.json() //to make it readable once the response returns to the client side. 
//     }).then(convertedData => {
//       //what would the response look like? 
//       //lets create a control structure to determine the proper procedure depending on the response 
//       if (convertedData === false) {
//         //lets allow the user to register an account. 
//         fetch('https://lit-reef-53083.herokuapp.com/api/users/register', {
//           //we will now describe the structure of our request for register
//           method: 'POST',
//           headers: {
//             'Content-Type': 'application/json'
//           },
//           //API only accepts request in a string format. 
//           body: JSON.stringify({
//             firstName: fName,
//             lastName: lastName,
//             email: email,
//             mobileNo: mobileNo,
//             password: password
//           })
//         }).then(res => {
//           console.log(res)
//           //console.log("hello");
//           return res.json()
//         }).then(data => {
//           console.log(data)
//           //lets create here a control structure to give out a proper response depending on the return from the backend. 
//           if (data === true) {
//             Swal.fire("New Account Registered Successfully!")
//           } else {
//             //inform the user that something went wrong
//             Swal.fire("Something with wrong during processing!")
//           }
//         }
//         )
//       } else {
//         //lets inform the user what went wrong 
//         Swal.fire("The Email Already exist!") // you can modify the message. 
//       }
//     }
//     )

//     //this block of code will run if the condition has been met
//     //how can we create a new account for user using the data that he/she entered?
//     //url -> describes the destination of the request. 
//     //3 STATES for a promise (pending, fullfillment, rejected)
//   } else {
//     Swal.fire("Oh no!!!")
//   }
// })

