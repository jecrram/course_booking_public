console.log("Hello");

// we captured the navsession element from the navbar component
let navItem = document.querySelector("#navSession");

let userToken = localStorage.getItem("token");

// lets create a control structure that will determine the display inside the navbar if there is a user currently logged in the app.

if (!userToken) {
  navItem.innerHTML = `
<li class="nav-item">
  <a href="./login.html" class="nav-link">
    Login
  </a>
</li>
`;
} else {
  navItem.innerHTML = `
  <li class="nav-item">
  <a href="./profile.html" class="nav-link">
    Profile
  </a>
  </li>

  <li class="nav-item">
    <a href="./logout.html" class="nav-link">
      Logout
    </a>
  </li>
  `

}
